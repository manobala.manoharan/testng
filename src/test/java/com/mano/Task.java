package com.mano;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

public class Task {
  @Test(groups={"Smoke Test"})
  public void createCustomer() {
	  System.out.println("Customer Name Has Created");
  }
  
  @Test(groups={"Regression Test"})
  public void newCustomer() {
	  System.out.println("New Customer Name Has Added");
  }
  
  @Test (groups={"Usability Testing"})
  public void ModifyCustomer() {
	  System.out.println("Customer Name Has Modified");
  }
  
  @Test(groups={"Smoke Test"})
  public void changeCustomer() {
	  System.out.println("Customer Name Has Changed successfully");
  }
  @BeforeClass
  public void beforeclassmethod() {
	  System.out.println("Before Class Will Execute Before Methods");
}
  
  @AfterClass
  public void afterclassmethod() {
	  System.out.println("After Class Will Execute After Methods");
}
}
