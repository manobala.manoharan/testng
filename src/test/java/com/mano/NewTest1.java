package com.mano;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest1 {
  @Test(priority = 2)
  public void modifyCustomer() {
	  System.out.println("The Customer Will Get Modified");
  }
  @Test(priority = 3)
  public void createCustomer() {
	  System.out.println("The Customer Will Get Created");
  }
  @Test(priority = 2)
  public void newCustomer() {
	  System.out.println("New Customer Will Get Created");
  }
  @BeforeMethod
  public void beforeCustomer() {
	  System.out.println("Verifying The Customer");
  }
  @AfterMethod
  public void afterCustomer() {
	  System.out.println("All The Transactions Are Done");
  }
  @BeforeClass
  public void beforeclassCustomer() {
	  System.out.println("Start Database Connection,Launch Browser");
  }
  @AfterClass
  public void afterclassCustomer() {
	  System.out.println("Close Database Connection,Close Browser");
  } 
}
